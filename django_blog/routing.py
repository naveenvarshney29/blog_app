from channels import include
channel_routing = [
    # This makes Django serve static files from settings.STATIC_URL, similar
    # to django.views.static.serve. This isn't ideal (not exactly production
    # quality) but it works for a minimal example.
    # Wire up websocket channels to our consumers:
    include('chat_app.routing.chat_app_routing',path=r'^/chat_app/'),
    include('chat_app.routing.user_comment_routing',path=r'^/post_detail'),
    include('chat_app.routing.blog_app_routing',path=r'^'),
    ]
