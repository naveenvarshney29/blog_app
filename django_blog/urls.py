"""django_blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from blog_app.views import IndexView,CreateUser,PostCreate,PostDetail,UserLogin
from blog_app import views
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',login_required(IndexView.as_view()),name='index'),
    url(r'^login/$', UserLogin.as_view(redirect_authenticated_user=True), name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^signup/$', CreateUser.as_view(), name='signup'),
    url(r'^create/$', PostCreate.as_view(), name='post'),
    url(r'^post_detail/(?P<pk>[0-9]+)/$', PostDetail.as_view(), name='post_detail'),
    url(r'^user_comment/$',views.user_comment, name='user_comment'),
    url(r'^comments/$',views.comments, name='comments'),
    url(r'^chat_app/',include('chat_app.urls',namespace='channel_app')),
]
