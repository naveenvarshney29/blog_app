from django.shortcuts import render
from django.views.generic import TemplateView,CreateView,ListView,DetailView
from django.core.urlresolvers import reverse_lazy
from blog_app import forms
from blog_app import models
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.contrib.auth.views import LoginView
from django.http import HttpResponse
from django.http import JsonResponse

# Create your views here.
class IndexView(ListView):
    """docstring for ."""
    model = models.UserPost
    template_name = 'index.html'
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(IndexView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['form'] = forms.PostCommentForm
        return context


class CreateUser(CreateView):
    """docstring for CreateStudent."""
    form_class = forms.UserCreateForm
    success_url = reverse_lazy("login")
    template_name = "signup.html"

class PostCreate(LoginRequiredMixin,CreateView):
    """docstring for ."""
    form_class = forms.UserPostForm
    template_name = 'post.html'
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return super().form_valid(form)


class PostDetail(LoginRequiredMixin,DetailView):
    """docstring for PostDetail."""
    template_name = 'post_detail.html'
    queryset = models.UserPost.objects.all()
    context_object_name='post_detail'
    def get_object(self):
        object = super(PostDetail,self).get_object()
        object.last_accessed = timezone.now()
        object.save()
        return object
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PostDetail, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['form'] = forms.PostCommentForm
        context['comments'] = models.PostComments.objects.filter(post_id= self.kwargs['pk'])
        print(context['comments'])
        return context
class UserLogin(LoginView):
    """docstring for UserLogin."""
    form_class = forms.UserLoginForm
    template_name = 'login.html'


def user_comment(request):
    print("hellll yeah================")
    insert_me = {"text": "hello i m from bot app"}
    post_id = request.POST['post_id']
    post_comment = request.POST['post_comment']
    print(post_id)
    print(post_comment)
    query = forms.PostCommentForm(request.POST)
    print(query)
    if query.is_valid():
        com  = models.PostComments()
        com.user_id = request.user
        com.post_id = models.UserPost.objects.get(pk=post_id)
        com.post_id.comments+=1
        com.post_id.save()
        print(com.post_id)
        com.post_comment = post_comment
        com.created_at = timezone.now()
        user = request.user
        com.save()
        print(com)
        return HttpResponse(True)
    return HttpResponse(False)
def comments(request):
    p_id = request.GET['id']
    comments = models.PostComments.objects.filter(post_id= p_id)
    post_comment = []
    post_id = []
    user_id = []
    for i in comments:
        post_comment.append(i.post_comment)
        user_id.append(i.user_id.username)
        print(i.post_comment,i.post_id,i.user_id)
    print("in comments")
    return JsonResponse({"post_comment":post_comment,"user_id":user_id})
