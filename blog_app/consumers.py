import json
from channels import Group
from .models import  UserPost
import logging
from channels.auth import channel_session_user, channel_session_user_from_http

log = logging.getLogger(__name__)
# The "slug" keyword argument here comes from the regex capture group in
# routing.py.
def connect_blog(message):
    """
    When the user opens a WebSocket to a liveblog stream, adds them to the
    group for that stream so they receive new post notifications.

    The notifications are actually sent in the Post model on save.
    """
    print("============connect_blog for blog=================")
    print("message",message['path'])
    log.debug("================i am in blog_app connect===============================")
    message.reply_channel.send({"accept": True})
    Group("real-update").add(message.reply_channel)


def disconnect_blog(message):
    """
    Removes the user from the liveblog group when they disconnect.

    Channels will auto-cleanup eventually, but it can take a while, and having old
    entries cluttering up your group will reduce performance.
    """
    print("================disconnect_blog for blog=================================")
    log.debug("====================i am in blog_app disconnect====================================")
    Group("real-update").discard(message.reply_channel)



#user_comment
def connect_comment(message):
    """
    When the user opens a WebSocket to a liveblog stream, adds them to the
    group for that stream so they receive new post notifications.

    The notifications are actually sent in the Post model on save.
    """
    print(message['path'])
    prefix, com_id = message['path'].strip('/').split('/')
    print("label = {} prefix= {}",com_id,prefix)
    print("============User_connect_comment=====================")
    log.debug("===============i am in user_comment connect========")
    message.reply_channel.send({"accept": True})
    Group("user-comment"+com_id,channel_layer=message.channel_layer).add(message.reply_channel)

def disconnect_comment(message):
    """
    Removes the user from the liveblog group when they disconnect.

    Channels will auto-cleanup eventually, but it can take a while, and having old
    entries cluttering up your group will reduce performance.
    """
    print("============User_disconnect_comment=====================")
    log.debug("===========i am in blog_app disconnect===========")
    Group("user-comment").discard(message.reply_channel)
#comment_notification
@channel_session_user_from_http
def connect_comment_notification(message):
    """
    When the user opens a WebSocket to a liveblog stream, adds them to the
    group for that stream so they receive new post notifications.

    The notifications are actually sent in the Post model on save.
    """
    print(message['path'])
    print("============connect_comment_notification=====================")
    log.debug("===============i am in connect_comment_notification========")
    message.reply_channel.send({"accept": True})
    print("=======user_id == ",message.user,message.user.id)
    Group("comment_notification"+message.user.id,channel_layer=message.channel_layer).add(message.reply_channel)


@channel_session_user
def disconnect_comment_notification(message):
    """
    Removes the user from the liveblog group when they disconnect.

    Channels will auto-cleanup eventually, but it can take a while, and having old
    entries cluttering up your group will reduce performance.
    """
    print("============disconnect_comment_notification=====================")
    log.debug("===========i am in disconnect_comment_notification===========")
    Group("comment_notification"+ message.user.id).discard(message.reply_channel)
