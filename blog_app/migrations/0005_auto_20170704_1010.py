# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-04 10:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog_app', '0004_auto_20170704_0440'),
    ]

    operations = [
        migrations.AddField(
            model_name='userpost',
            name='comments',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='userpost',
            name='likes',
            field=models.IntegerField(default=0),
        ),
    ]
