from django.db import models
from django.contrib.auth.models import User,PermissionsMixin
from django.core.urlresolvers import reverse
# Create your models here.
from django.contrib import auth
from channels import Group
import json


class User(User,PermissionsMixin):
    def __str__(self):
        return "@{}".format(self.username)



class UserPost(models.Model):
    """docstring for ."""
    user = models.ForeignKey('auth.User',related_name="userpost",on_delete=models.CASCADE)
    post_title = models.CharField(max_length=100)
    post_text = models.TextField()
    created_at = models.DateTimeField(auto_now=True)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)
    def __str__(self):
        return self.post_title + " By " + self.user.first_name
    def get_absolute_url(self):
        return reverse('post_detail',args=(self.id,))

    def send_notification(self):
        """
        Sends a notification to everyone in our Liveblog's group with our
        content.
        """
        # Make the payload of the notification. We'll JSONify this, so it has
        # to be simple types, which is why we handle the datetime here.
        print("=======send_notification post called===================")
        notification = {
            "id": self.id,
            "user": self.user.first_name,
            "title": self.post_title,
            "created": self.created_at.strftime("%a %d %b %Y %H:%M"),
        }
        # Encode and send that message to the whole channels Group for our
        # liveblog. Note how you can send to a channel or Group from any part
        # of Django, not just inside a consumer.
        Group("real-update").send({
            # WebSocket text frame, with JSON content
            "text": json.dumps(notification),
        })

    # def save(self, *args, **kwargs):
    #     """
    #     Hooking send_notification into the save of the object as I'm not
    #     the biggest fan of signals.
    #     """
    #     print("-----------Userpost save called-----------------------")
    #     result = super(UserPost, self).save(*args, **kwargs)
    #     self.send_notification()
    #     return result




class PostLikes(models.Model):
    """docstring for ."""
    post_id = models.ForeignKey(UserPost)
    user_id = models.ForeignKey('auth.User')
    val = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return str(self.val) + " by " + self.user_id.first_name




class PostComments(models.Model):
    """docstring for ."""
    post_id = models.ForeignKey(UserPost)
    user_id = models.ForeignKey('auth.User')
    post_comment = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.post_comment + " by " + self.user_id.first_name

    # def send_comment(self):
    #     """
    #     Sends a notification to everyone in our Liveblog's group with our
    #     content.
    #     """
    #     # Make the payload of the notification. We'll JSONify this, so it has
    #     # to be simple types, which is why we handle the datetime here.
    #     print("----------------------send_comment called-----------------")
    #     comment = {
    #         "user_id": self.user_id.first_name,
    #         "post_comment": self.post_comment,
    #         "created": self.created_at.strftime("%a %d %b %Y %H:%M"),
    #     }
    #     # Encode and send that message to the whole channels Group for our
    #     # liveblog. Note how you can send to a channel or Group from any part
    #     # of Django, not just inside a consumer.
    #     Group("user-comment").send({
    #         # WebSocket text frame, with JSON content
    #         "text": json.dumps(comment),
    #     })
    # def save(self,*args,**kwargs):
    #     print("-----------post_comment save called-----------------------")
    #     result = super(PostComments, self).save(*args, **kwargs)
    #     self.send_comment()
    #     return result
