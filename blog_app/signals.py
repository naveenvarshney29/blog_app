from django.db.models.signals import post_save
from django.dispatch import receiver
from blog_app.models import UserPost,PostComments,PostLikes
from channels import Group
import json

@receiver(post_save,sender=UserPost)
def send_notification(sender, instance, created, **kwargs):
    """
    Sends a notification to everyone who are on index page with our
    content.
    """
    # Make the payload of the notification. We'll JSONify this, so it has
    # to be simple types, which is why we handle the datetime here.
    if created:
        print(instance)
        print("==================")
        print(instance.id,instance.user.first_name,instance.post_title,instance.created_at)
        print("=======send_notification post called===================")
        notification = {
            "id": instance.id,
            "user": instance.user.first_name,
            "title": instance.post_title,
            "created": instance.created_at.strftime("%a %d %b %Y %H:%M"),
        }
        # Encode and send that message to the whole channels Group for our
        # liveblog. Note how you can send to a channel or Group from any part
        # of Django, not just inside a consumer.
        Group("real-update").send({
            # WebSocket text frame, with JSON content
            "text": json.dumps(notification),
        })


@receiver(post_save,sender=PostComments)
def send_comment(sender, instance, created, **kwargs):
    """
    Sends a notification to everyone in our Liveblog's group with our
    content.
    """
    # Make the payload of the notification. We'll JSONify this, so it has
    # to be simple types, which is why we handle the datetime here.
    if created:
        print("----------------------send_comment called-----------------")
        comment = {
        "user_id": instance.user_id.first_name,
        "post_comment": instance.post_comment,
        "created": instance.created_at.strftime("%a %d %b %Y %H:%M"),
        }
        # Encode and send that message to the whole channels Group for our
        # liveblog. Note how you can send to a channel or Group from any part
        # of Django, not just inside a consumer.

        Group("user-comment"+ str(instance.post_id.id)).send({
        # WebSocket text frame, with JSON content
        "text": json.dumps(comment),
        })
        Group("comment_notification"+ str(instance.post_id.user.id)).send({
        # WebSocket text frame, with JSON content
        "text": json.dumps(comment),
        })
