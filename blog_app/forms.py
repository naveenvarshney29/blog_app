from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django import forms
from . import models


class UserCreateForm(UserCreationForm):
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder': 'Password'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Re enter your password'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}))
    class Meta:
        fields = ("first_name","last_name","username", "email", "password1", "password2")
        model = User
        widgets = {
        'first_name' : forms.TextInput(attrs={'class' : 'form-control','autofocus':'true','placeholder':'First Name'}),
        'last_name' : forms.TextInput(attrs={'class' : 'form-control','placeholder':'Last Name'}),
        'email' : forms.EmailInput(attrs={'class' : 'form-control','placeholder':'Email'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["username"].label = "Display name"
        self.fields["email"].label = "Email address"
        self.fields["password1"].label = "Password"
        self.fields["password2"].label = "Password confirmation"


class UserPostForm(forms.ModelForm):
    """docstring for UserPostForm."""
    class Meta:
        """docstring for Meta."""
        model = models.UserPost
        fields = ('post_title','post_text')
        widgets ={
        'post_title': forms.TextInput(attrs={'class':'form-control'}),
        'post_text': forms.Textarea(attrs={'class':'form-control'}),
        }
class UserLoginForm(AuthenticationForm):
    """docstring for UserLoginForm."""
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Password'}))
    class Meta:
        model = User


class PostCommentForm(forms.ModelForm):
    """docstring for PostCommentForm."""
    post_comment = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Type a Comment here','id':'p_com','autocomplete':'off'}))
    class Meta:
        model = models.PostComments
        fields = ('post_comment',)
