# from blog_app.consumers import connect_blog, disconnect_blog
# from channels.routing import route
#
# blog_app_routing = [
#     # This makes Django serve static files from settings.STATIC_URL, similar
#     # to django.views.static.serve. This isn't ideal (not exactly production
#     # quality) but it works for a minimal example.
#     # Wire up websocket channels to our consumers:
#     route('websocket.connect', connect_blog,path=r'^$'),
#     route('websocket.disconnect', disconnect_blog,path=r'^$'),
#     # route("websocket.receive", ws_receive),
#     ]
