from django.contrib import admin

# Register your models here.
from .models import Room,Message


admin.site.register(
    Room,
    list_display=["id", "name", "label"],
    list_display_links=["id", "name"],
)
admin.site.register(Message)
