from chat_app.consumers import ws_connect, ws_disconnect,ws_receive
from blog_app.consumers import connect_blog, disconnect_blog,connect_comment,disconnect_comment,connect_comment_notification,disconnect_comment_notification
from channels.routing import route

chat_app_routing = [
    # This makes Django serve static files from settings.STATIC_URL, similar
    # to django.views.static.serve. This isn't ideal (not exactly production
    # quality) but it works for a minimal example.
    # Wire up websocket channels to our consumers:
    route('websocket.connect', ws_connect),
    route('websocket.disconnect', ws_disconnect),
    route("websocket.receive", ws_receive),

    ]
user_comment_routing = [
    # This makes Django serve static files from settings.STATIC_URL, similar
    # to django.views.static.serve. This isn't ideal (not exactly production
    # quality) but it works for a minimal example.
    # Wire up websocket channels to our consumers:
    route('websocket.connect', connect_comment),
    route('websocket.disconnect', disconnect_comment),
    # route("websocket.receive", ws_receive),
    ]
blog_app_routing = [
    # This makes Django serve static files from settings.STATIC_URL, similar
    # to django.views.static.serve. This isn't ideal (not exactly production
    # quality) but it works for a minimal example.
    # Wire up websocket channels to our consumers:
    route('websocket.connect', connect_blog),
    route('websocket.disconnect', disconnect_blog),
    # route("websocket.receive", ws_receive),
    ]
comment_notification_routing=[
route('websocket.connect', connect_comment_notification,path='^comment_notification/$'),
route('websocket.disconnect', disconnect_comment_notification,path='^comment_notification/$'),
]
